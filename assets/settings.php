// Read the file salt.txt for the hash salt. This file should be changed for
// each site.
$settings['hash_salt'] = file_get_contents('/app/assets/salt.txt');

// Keep site configuration outside the document root.
$settings['config_sync_directory'] = '../config/sync';

// Include Lando configuration.
if (file_exists($app_root . '/' . $site_path . '/settings.lando.php')) {
  include $app_root . '/' . $site_path . '/settings.lando.php';
}

// Include local configuration.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
